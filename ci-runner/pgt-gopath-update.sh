#!/bin/bash
# Not safe for concurrent execution: wrap in flock(1) or a systemd service.
set -e

cd /srv/gopath

# Create a new src-<timestamp> directory:
latest=$(/usr/bin/docker run -v /srv/gopath:/srv/gopath --network=host registry.salsa.debian.org/go-team/infra/pkg-go-tools/pgt-gopath:1240c033 -debian_mirror=http://localhost:3142/deb.debian.org/debian)

# Atomically update the src symlink:
ln -snf src-${latest} new_src
mv -T new_src src

# Clean up all old src-<timestamp> directories:
rm -rf -- $(ls -d src-* | grep -v "^src-$latest\$")
