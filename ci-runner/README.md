# ci-runner

Here's how to bootstrap a working ci runner, you need `root` rights on the host
target.

## Install ansible on the machine

```sh
$ apt-get update && apt-get install -y ansible
```

You can also install it with pip, see [https://docs.ansible.com/ansible/latest/installation\_guide/intro\_installation.html#pip-install](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#pip-install).

## Download the Docker role

```sh
$ ansible-galaxy install geerlingguy.docker
```

## Create the provision file

Use this file to indicate which hosts will be provisioned and which Salsa project/group they will be registered to. The file shall be named `.inventory` and look as follows:


```
[ci_runner]
<REMOTE_IP>
...

[ci_runner:vars]
gitlab_registration_token = "<TOKEN>"
gitlab_description = "<DESCRIPTION>"
```

Replace there the following placeholders:

* `<REMOTE_IP>`: the host where you want to deploy the ci runner
* `<TOKEN>`: the registration token you find at the project/group CI/CD settings (ex. [https://salsa.debian.org/groups/go-team/-/settings/ci_cd](https://salsa.debian.org/groups/go-team/-/settings/ci_cd))
* `<DESCRIPTION>`: a short text helping users to identify this runner (ex. in the job pages)

The `gitlab_description` variable is optional, if missing "Go team runner" will be used. Also `gitlab_registration_token` is optional, if missing the registration to Gitlab will be skipped.

## Run ansible-playbook

```sh
$ ansible-playbook -i .inventory ci-runner.yaml -C # check mode (dry-run)
$ ansible-playbook -i .inventory ci-runner.yaml
```

Depending on how you connect on the remote host (root or normal user), you may
want to use one of these options:

```txt
  -K, --ask-become-pass
                        ask for privilege escalation password
  -b, --become          run operations with become (does not imply password
                        prompting)
```

## Running in a virtual machine

For security reasons or for testing purposes you may deploy the ci runner on a virtual machine.

To help in this, a `Vagrantfile` is provided; deploying and provisioning a new runner is as easy as executing `vagrant up`.

Vagrant takes care of the Ansible playbook for you but still needs the `.inventory` file as defined above (actually the `[ci_runner]` group is rewritten on-the-fly by Vagrant with the newly created VM's IP address).

Review the resources assigned by `Vagrantfile` (CPUs, memory, storage for `/srv`), ensure your host has the needed capacity. CPU and memory affect the speed, you can lower them at the price of a slower runner but the storage is less flexible, although it is actually allocated on demand.

Enjoy :)
