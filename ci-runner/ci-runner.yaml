---
- name: Update and upgrade packages
  hosts: ci_runner
  tasks:
    - apt:
        upgrade: true
        update_cache: true

- name: Install base packages
  hosts: ci_runner
  tasks:
    - apt:
        pkg:
          - apt-cacher-ng
          - unattended-upgrades
          - vim

- name: Install Docker engine
  hosts: ci_runner
  roles:
    - geerlingguy.docker

- name: Setup pgt-gopath
  hosts: ci_runner
  tasks:
    - name: Create /srv/gopath
      file:
        path: /srv/gopath
        state: directory
        owner: "1042"
        group: "1042"
        mode: 0775
        recurse: true
        follow: false
    - name: Copy the update script
      copy:
        src: ./pgt-gopath-update.sh
        dest: /srv/gopath/update.sh
        group: root
        owner: root
        mode: 0770
    - name: Copy the pgt-gopath-update service file
      copy:
        src: ./pgt-gopath-update.service
        dest: /etc/systemd/system/pgt-gopath-update.service
    - name: Copy the pgt-gopath-update timer file
      copy:
        src: ./pgt-gopath-update.timer
        dest: /etc/systemd/system/pgt-gopath-update.timer
    - name: Start and enable the pgt-gopath-update timer service
      service:
        name: pgt-gopath-update.timer
        state: started
        enabled: true
      when: not ansible_check_mode

- name: Setup the Gitlab runner
  hosts: ci_runner
  tasks:
    - name: Create the /srv/cache directory
      file:
        path: /srv/cache
        state: directory
        owner: "1042"
        group: "1042"
        mode: 0775
        recurse: true
    - name: Create the /srv/cache/overlay directory
      file:
        path: /srv/cache/overlay
        state: directory
        owner: "1042"
        group: "1042"
        mode: 0775
        recurse: true
    - name: Create the config directory
      file:
        path: /srv/gitlab-runner/config
        state: directory
        mode: 0775
        recurse: true
    - name: Register the Gitlab runner
      command:
        cmd: >
          docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
            --non-interactive
            --executor "docker"
            --docker-image "set by pkg-go-tools/pipeline/test-archive.yml"
            --docker-volumes "/srv/gopath:/srv/gopath"
            --docker-volumes "/srv/cache:/cache"
            --docker-network-mode host
            --docker-privileged=true
            --url "https://salsa.debian.org/"
            --tag-list "go-ci"
            --run-untagged=false
            --registration-token "{{ gitlab_registration_token }}"
            --description "{{ gitlab_description | default('Go team runner', true) }}"
        creates: /srv/gitlab-runner/config/config.toml
      when: not ansible_check_mode and gitlab_registration_token is defined
      register: runner_registration
    - name: Copy the Systemd service file
      copy:
        src: ./gitlab-runner.service
        dest: /etc/systemd/system/gitlab-runner.service
    - name: Enable the Gitlab runner service
      service:
        name: gitlab-runner.service
        enabled: true
      when: not ansible_check_mode
    - name: Start the Gitlab runner service
      service:
        name: gitlab-runner.service
        state: started
      when: not ansible_check_mode and runner_registration.changed == true
